import re

from dcim.models import Device, Region, SiteGroup, Site, Rack, VirtualChassis
from extras.scripts import *

__version__ = "0.1.0"
__author__ = "Roman Mariancik"

JOB_TIMEOUT = 500


# WORKING REGEX FOR CISCO SWITCHES
# (?P<name>GigabitEthernet|Te|StackPort)(?P<pos>\d+)(?P<interface>.+)
class VirtualChassisCreator(Script):
    job_timeout = JOB_TIMEOUT

    class Meta:
        name = "Virtual Chassis Creator"
        description = "Virtual Chassis Creator"
        field_order = ["name"]

    chassis_name = StringVar(description="Virtual chassis name")
    chassis_domain = StringVar(required=False)
    chassis_description = StringVar(required=False)

    region = ObjectVar(
        model=Region,
        required=False,
    )
    site_group = ObjectVar(
        model=SiteGroup,
        required=False,
    )
    site = ObjectVar(
        model=Site,
        required=False,
    )
    rack = ObjectVar(
        model=Rack,
        required=False,
    )
    master = ObjectVar(
        model=Device,
        required=False
    )
    members = MultiObjectVar(
        model=Device,
        required=False,
        label="Virtual chassis members",
    )
    initial_position = IntegerVar(default=1,
                                  description="Position of the first member device. "
                                              "Increases by one for each additional member.",
                                  required=False)

    interface_regex = StringVar(required=False, description="Regex used to rename interfaces.")

    def run(self, data: dict, commit):
        chassis = VirtualChassis.objects.create(name=data["chassis_name"],
                                                domain=data["chassis_domain"],
                                                description=data["chassis_description"])
        chassis.members.set(data["members"])
        pos = data["initial_position"]
        # pattern = repr(data["interface_regex"].replace("{pos}", "[0-9]*"))
        regex = re.compile(data["interface_regex"])
        for member in chassis.members.all():
            member.vc_position = pos
            for interface in member.interfaces.all():
                # new_name_pattern = repr(data["interface_regex"].format(pos=pos))
                result = regex.match(interface.name)
                if result:
                    try:
                        new_if_name = f"{result.group('name')}{pos}{result.group('interface')}"
                        interface.name = new_if_name
                        interface.save()
                        print(new_if_name)
                    except IndexError:
                        print(f"No such group found for {result}")
            pos += 1
            member.save()

        chassis.master = data["master"]
        chassis.save()
        return f"Chassis \'{chassis.name}\' successfully created!"
