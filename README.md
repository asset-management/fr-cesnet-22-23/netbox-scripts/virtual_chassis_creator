# Virtual Chassis Creator
Tento skript je možné využiť na vytvorenie virtuálných šasi (stack) v Netboxe 
a súčasné premenovanie rozhraní podľa poradia v stacku. 

## Regex
Názov rozhraní je uprovavaný pomocou regexu z balíčka **re** v Pythone.
Názov rozhrania je možné vyskladať z troch premenných

- ```name```
- ```pos```
- ```interface```

Premenná ```pos``` sa vždy nahradí aktuálnou pozíciou zariadenia. 
Poradie zariadení je totožné s poradím v akom boli 
vložené do poľa **members**.

Hodnota premennej ```pos``` začína na hodnote poľa **initial position** 
a prechodom na ďalší switch je zvýšená o jedna.

### Príklad regexu
```(?P<name>GigabitEthernet|Te|StackPort)(?P<pos>\d+)(?P<interface>.+)```
- ```()``` rozdelia výsledok na skupiny. Skript podporuje 3 skupiny (```name, pos, interface```).
- ```?P<var>``` definuje skupinu, ktorej hodnota bude uložená v premennej ```var```.
- ```|``` umožňuje alternatívu, v príklade umožňuje premenovať rozhrania s názvami *GigabitEthernet*, *Te* alebo *StackPort*.
- ```\d+``` vynucuje zhodu na pozitívnom decimálnom čísle.
- Kompletná dokumentácia balíčku **re** je dostupná na: [https://docs.python.org/3/library/re.html]()

## Príklad konfigurácie

![](images/vc.webm){width="100%" height="100%"}
